package com.example.user.jobqueue.jobs;

import com.example.user.jobqueue.controllers.TwitterController;
import com.example.user.jobqueue.entity.Tweet;
import com.example.user.jobqueue.events.PostedTweetEvent;
import com.example.user.jobqueue.events.PostingTweetEvent;
import com.example.user.jobqueue.models.TweetModel;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import org.greenrobot.eventbus.EventBus;

import twitter4j.Status;
import twitter4j.TwitterException;

import java.util.Date;

public class PostTweetJob extends Job {
    private long localId;
    private String text;
    public PostTweetJob(String text) {
        super(new Params(Priority.MID).requireNetwork().persist().groupBy("post_tweet"));//order of tweets matter, we don't want to send two in parallel
        //use a negative id so that it cannot collide w/ twitter ids
        //we have to set local id here so it gets serialized into job (to find tweet later on)
        localId = -System.currentTimeMillis();
        this.text = text;
    }

    @Override
    public void onAdded() {
        //job has been secured to disk, add item to database
        try {
            Tweet tweet = new Tweet(
                    localId,
                    null,
                    text,
                    TwitterController.getInstance().getUserId(),
                    null,
                    new Date(System.currentTimeMillis())
            );
            TweetModel.getInstance().insertOrReplace(tweet);
            EventBus.getDefault().post(new PostingTweetEvent(tweet));
        } catch (TwitterException exception) {
            //if we cannot get user id, we won't add it locally for now.
        }
    }

    @Override
    public void onRun() throws Throwable {
        Status status = TwitterController.getInstance().postTweet(text);
        Tweet newTweet = new Tweet(status);
        TweetModel tweetModel = TweetModel.getInstance();
        Tweet existingTweet = tweetModel.getTweetByLocalId(localId);
        if(existingTweet != null) {
            existingTweet.updateNotNull(newTweet);
            //don't set local to false. this way, next time we ask for history update, we'll send proper tweet id
            tweetModel.insertOrReplace(existingTweet);
        } else {
            //somewhat local tweet does not exist. we might have crashed before onAdded is called.
            //just insert as if it is a new tweet
            tweetModel.insertOrReplace(newTweet);
        }
        EventBus.getDefault().post(new PostedTweetEvent(newTweet, localId));
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }


}

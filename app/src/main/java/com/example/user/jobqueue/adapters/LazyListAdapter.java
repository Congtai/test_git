package com.example.user.jobqueue.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import de.greenrobot.dao.query.LazyList;

/**
 * Created by user on 5/1/16.
 */
public class LazyListAdapter<T> extends BaseAdapter {

    LazyList<T> lazyList = null;

    public LazyListAdapter() {

    }

    public LazyListAdapter(LazyList<T> newlist) {
        this.lazyList = newlist;
    }

    public void replaceLazyList(LazyList<T> newlist) {
        if (lazyList != null) {
            lazyList.close();
        }
        lazyList = newlist;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return lazyList == null ? 0 : lazyList.size();
    }

    @Override
    public T getItem(int position) {
        return lazyList == null ? null : lazyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    public void close() {
        if (lazyList != null) {
            lazyList.close();
            lazyList = null;
        }


    }
}

package com.example.user.jobqueue.jobs;

import com.example.user.jobqueue.controllers.TwitterController;
import com.example.user.jobqueue.entity.Tweet;
import com.example.user.jobqueue.events.FetchedNewTweetsEvent;
import com.example.user.jobqueue.models.TweetModel;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import org.greenrobot.eventbus.EventBus;

import twitter4j.Status;
import twitter4j.TwitterException;

import java.util.ArrayList;
import java.util.List;


public class FetchTweetsJob extends Job {

    public FetchTweetsJob() {
        //use singleWith so that if the same job has already been added and is not yet running,
        //it will only run once.
        super(new Params(Priority.HIGH).requireNetwork().persist());
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        TweetModel tweetModel = TweetModel.getInstance();
        Tweet lastTweet = tweetModel.getLastTweet();
        List<Status> statusList = TwitterController.getInstance().loadTweets(lastTweet == null ? null : lastTweet.getServerId());
        if(statusList.size() > 0) {
            List<Tweet> tweets = new ArrayList<Tweet>(statusList.size());
            for(Status status : statusList) {
                Tweet tweet = new Tweet(status);
                tweets.add(tweet);
            }
            tweetModel.insertOrReplaceAll(tweets);
            EventBus.getDefault().post(new FetchedNewTweetsEvent());
        }
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {

        return false;
    }


}

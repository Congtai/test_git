package com.example.user.jobqueue.events;

public class DeletedTweetEvent {
    private long id;
    public DeletedTweetEvent(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}

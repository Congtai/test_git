package com.example.user.jobqueue.entity;

import twitter4j.auth.AccessToken;
import twitter4j.Status;

/**
 * Created by user on 5/1/16.
 */
public class Tweet extends TweetBase {
    public Tweet() {

    }

    public Tweet(Long localId) {
        super(localId);
    }

    public Tweet(Long localId, Long serverId, String text, Long userId, Boolean isLocal, java.util.Date createdAt) {
        super(localId, serverId, text, userId, isLocal, createdAt);
    }

    // KEEP METHODS - put your custom methods here
    public Tweet(Status status) {
        super(null, status.getId(), status.getText(), status.getUser().getId(), false, status.getCreatedAt());
    }

    @Override
    public void onBeforeSave() {
        isLocal = serverId == null;
        super.onBeforeSave();
    }
}
